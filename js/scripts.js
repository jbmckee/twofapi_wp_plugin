jQuery(function() {
	jQuery("#2fapi form").submit(function () {
		jQuery('body').loadingModal({
			text: 'Updating Data'
		});
	})

	jQuery("#loginform").submit(function (e) {
		jQuery('#login_status').text('Attempting Login');

		jQuery.ajax({
			type: 'POST',
			dataType: 'json',
			url: ajax_login_object.ajaxurl,
			cache: false,
			data: {
				'action': 'ajaxlogin', //calls wp_ajax_nopriv_ajaxlogin
				'username': jQuery('#user_login').val(),
				'password': jQuery('#user_pass').val(),
				'security': ajax_login_object.security,
				'rememberme': jQuery('#rememberme').is(':checked')
				}
			})
			.done(function(data){
				console.log(data);
				if (data.ip_address !== ajax_login_object.ip_address){
					alert("Security Error 1");
				}
				else {
					if (data.already === true) {
						top.location.href = ajax_login_object.redirecturl;
					}
					else if (data.loggedin === true) {

						jQuery.data = data;

						jQuery("#loginform > p").hide('slow');
						jQuery("#login_status").hide();
						jQuery("input[name='twofapi-group']").prop('checked', false);
						jQuery("#twofapi-choose").show('slow');
					}
					else {
						jQuery('#login_status').html(data.message);
					}
				}
			})
			.fail(function( jqXHR, textStatus ) {
				alert( "Request failed: " + textStatus );
			});
		e.preventDefault();

	});

	jQuery("input[name='twofapi-group']").click(function () {
		jQuery.radioValue = jQuery("input[name='twofapi-group']:checked").val();
		if (jQuery.radioValue === 'qr'){
			do_qr();
		}
		else if (jQuery.radioValue === 'sms'){
			do_sms();
		}
		else if (jQuery.radioValue === 'auth'){
			device_auth();
		}
	});

	jQuery('#twofapi-show-input').click(function () {
		jQuery.use_vcode = true;
		jQuery('#twofapi-show-input-link').hide('fast');
		jQuery("#twofapi-vcode").val('');
		jQuery('#twofapi-show-codebox').show('fast');
	});

	jQuery('.twofapi_check').click(function () {
		jQuery('#login_status').text('Checking Code').show();
		if (jQuery.radioValue === 'qr') {
			wait_qr(0)
		}
		else if (jQuery.radioValue === 'sms'){
			check_sms();
		}
	})

	jQuery('#twofapi_verify_vcode').click(function () {
		var vcode = jQuery("#twofapi-vcode").val();
		if (vcode) {
			jQuery('#login_status').text('Checking Code').show();
			do_vcode(vcode);
		}
		else{
			jQuery('#login_status').text('Please enter 7 digit verification code below.').show();
		}
	})
});

jQuery.use_vcode = false;

function do_sms() {
	jQuery('#login_status').text('Sending SMS message.').show();
	jQuery('#twofapi-choose').hide('fast');
	jQuery('#twofapi-qr-show').hide('fast');
	jQuery('#twofapi-qr-img').attr("src", '');

	jQuery.ajax({
		type: 'POST',
		dataType: 'json',
		url: ajax_login_object.ajaxurl,
		cache: false,
		data: {
			'action': 'ajax_do_sms',
			'token': jQuery.data.token
		}
	})
	.done(function(data){
		console.log(data);
		if (data.ip_address !== ajax_login_object.ip_address){
			alert("Security Error 5");
		}
		else {
			if (data.errors){
				var error_strgfy = JSON.stringify(data.errors);
				var error_str = error_strgfy.replace(/:/g," ");
				error_str = error_str.replace(/[^0-9a-zA-Z_ ]/g,"");
				jQuery('#login_status').text(error_str).show();
				jQuery("input[name='twofapi-group']").prop('checked', false);
				jQuery('#twofapi-choose').show('fast');
			}
			else if (data.error){
				jQuery('#login_status').text(data.msg).show();
				jQuery("input[name='twofapi-group']").prop('checked', false);
				jQuery('#twofapi-choose').show('fast');
			}
			else if (data.timeout){
				jQuery('#login_status').text('Auth Timed Out').show();
				jQuery('#twofapi-show-sms-check').show();
				jQuery.verify_key = data.timeout;
			}
			else if (data.valid){
				real_login();
			}
			else{
				alert('SMS Error');
			}
		}

	})
	.fail(function( jqXHR, textStatus ) {
		alert( "Request failed: " + textStatus );
	});
}

function check_sms() {
	jQuery('#login_status').text('Checking SMS Response.').show();

	if (!jQuery.verify_key){
		alert("Error: SMS Key missing.");
		return false;
	}

	jQuery.ajax({
		type: 'POST',
		dataType: 'json',
		url: ajax_login_object.ajaxurl,
		cache: false,
		data: {
			'action': 'ajax_do_sms',
			'token': jQuery.data.token,
			'verify_key': jQuery.verify_key
		}
	})
	.done(function(data){
		if (data.ip_address !== ajax_login_object.ip_address){
			alert("Security Error 5");
		}
		else {
			if (data.errors){
				var error_strgfy = JSON.stringify(data.errors);
				var error_str = error_strgfy.replace(/:/g," ");
				error_str = error_str.replace(/[^0-9a-zA-Z_ ]/g,"");
				jQuery('#login_status').text(error_str).show();
				jQuery("input[name='twofapi-group']").prop('checked', false);
				jQuery('#twofapi-choose').show('fast');
			}
			else if (data.error){
				jQuery('#login_status').text(data.msg).show();
				jQuery("input[name='twofapi-group']").prop('checked', false);
				jQuery('#twofapi-choose').show('fast');
			}
			else if (data.valid){
				real_login();
			}
			else{
				jQuery('#login_status').text('SMS Not Yet Verified.').show();
			}
		}
	})
	.fail(function( jqXHR, textStatus ) {
		alert( "Request failed: " + textStatus );
	});
}


function do_qr() {
	jQuery('#login_status').text('Generating QR Code').show();

	jQuery.ajax({
		type: 'POST',
		dataType: 'json',
		url: ajax_login_object.ajaxurl,
		cache: false,
		data: {
			'action': 'ajax_get_qr',
			'token': jQuery.data.token,
		}
	})
	.done(function(data){
		console.log(data);
		if (data.error){
			jQuery('#login_status').text(data.msg).show();
			jQuery("input[name='twofapi-group']").prop('checked', false);
			jQuery('#twofapi-choose').show('fast');
		}
		else if (data.errors){
			var error_strgfy = JSON.stringify(data.errors);
			var error_str = error_strgfy.replace(/:/g," ");
			error_str = error_str.replace(/[^0-9a-zA-Z_ ]/g,"");
			jQuery('#login_status').text(error_str).show();
			jQuery('#twofapi-qr-show').hide('fast');
			jQuery('#twofapi-qr-img').attr("src", '');
			jQuery("input[name='twofapi-group']").prop('checked', false);
			jQuery('#twofapi-choose').show('fast');
		}
		else if (data.ip_address !== ajax_login_object.ip_address){
			alert("Security Error 3");
		}
		else {
			jQuery('#login_status').hide();
			jQuery('#twofapi-choose').hide('fast');
			jQuery('#twofapi-qr-show').show('fast');
			jQuery('#twofapi-qr-img').attr("src", data.qr_img_src);
			jQuery.verify_key = data.verify_key;
			wait_qr(1);
		}

	})
	.fail(function( jqXHR, textStatus ) {
		alert( "Request failed: " + textStatus );
	});
}

function wait_qr(wait) {
	jQuery.ajax({
		type: 'POST',
		dataType: 'json',
		url: ajax_login_object.ajaxurl,
		timeout: 95000,
		cache: false,
		data: {
			'action': 'ajax_wait_qr',
			'token': jQuery.data.token,
			'wait': wait,
			'verify_key': jQuery.verify_key
		}
	})
	.done(function(data){
		console.log(data);
		if (data.error){
			jQuery('#login_status').text(data.msg).show();
			jQuery("input[name='twofapi-group']").prop('checked', false);
			jQuery('#twofapi-choose').show('fast');
		}
		else if (data.ip_address !== ajax_login_object.ip_address){
			alert("Security Error 3");
		}
		else {
			if (jQuery.use_vcode){
				return false;
			}
			if (data.errors){
				var error_strgfy = JSON.stringify(data.errors);
				var error_str = error_strgfy.replace(/:/g," ");
				error_str = error_str.replace(/[^0-9a-zA-Z_ ]/g,"");
				jQuery('#login_status').text(error_str).show();
				jQuery('#twofapi-qr-show').hide('fast');
				jQuery('#twofapi-qr-img').attr("src", '');
				jQuery("input[name='twofapi-group']").prop('checked', false);
				jQuery('#twofapi-choose').show('fast');
			}
			else if (data.timeout){
				if (!jQuery.use_vcode) {
					jQuery('#twofapi-show-check-link').show('fast');
				}
			}
			else if (data.valid){
				real_login();
			}
			else{
				alert('QR Code Error');
			}
		}
	})
	.fail(function( jqXHR, textStatus ) {
		if (!jQuery.use_vcode) {
			alert("QR Code Validation Failed: " + textStatus);
		}
	});
}

function do_vcode(vcode) {
	jQuery.ajax({
		type: 'POST',
		dataType: 'json',
		url: ajax_login_object.ajaxurl,
		timeout: 95000,
		cache: false,
		data: {
			'action': 'ajax_vcode',
			'token': jQuery.data.token,
			'vcode': vcode,
			'verify_key': jQuery.verify_key
		}
	})
	.done(function(data){
		console.log(data);
		if (data.error){
			jQuery('#login_status').text(data.msg).show();
			jQuery("input[name='twofapi-group']").prop('checked', false);
			jQuery('#twofapi-choose').show('fast');
		}
		else if (data.ip_address !== ajax_login_object.ip_address){
			alert("Security Error 4");
		}
		else {
			if (data.errors){
				var error_strgfy = JSON.stringify(data.errors);
				var error_str = error_strgfy.replace(/:/g," ");
				error_str = error_str.replace(/[^0-9a-zA-Z_ ]/g,"");
				jQuery('#login_status').text(error_str).show();
				jQuery('#twofapi-qr-show').hide('fast');
				jQuery('#twofapi-qr-img').attr("src", '');
				jQuery("input[name='twofapi-group']").prop('checked', false);
				jQuery('#twofapi-choose').show('fast');
			}
			else if (data.timeout){
				jQuery('#twofapi-show-check-link').show('fast');
			}
			else if (data.valid){
				real_login();
			}
			else{
				alert('QR Code Error');
			}
		}
	})
	.fail(function( jqXHR, textStatus ) {
		alert( "QR Code Validation Failed: " + textStatus );
	});
}

function real_login() {
	jQuery('#login_status').text('Authentication Successfull - Redirecting').show();

	jQuery.ajax({
		type: 'POST',
		dataType: 'json',
		url: ajax_login_object.ajaxurl,
		data: {
			'action': 'ajax_real_login',
			'username': jQuery('#user_login').val(),
			'password': jQuery('#user_pass').val(),
			'security': ajax_login_object.ip_address,
			'rememberme': jQuery('#rememberme').is(':checked')
		}
	})
	.done(function(data){
		if (data.ip_address !== ajax_login_object.ip_address){
			alert("Security Error 4");
		}
		else {
			if (data.loggedin === true) {
				document.location.href = ajax_login_object.redirecturl;
			}
			else {
				jQuery('#login_status').html(data.message);
			}
		}
	})
	.fail(function( jqXHR, textStatus ) {
		alert( "Request failed: " + textStatus );
	});

}

function device_auth() {
	jQuery('#login_status').text('Processing Device Authentication.').show();
	jQuery('#twofapi-choose').hide('fast');
	jQuery('#twofapi-qr-show').hide('fast');
	jQuery('#twofapi-qr-img').attr("src", '');

	jQuery.ajax({
		type: 'POST',
		dataType: 'json',
		url: ajax_login_object.ajaxurl,
		cache: false,
		data: {
			'action': 'ajax_device_auth',
			'user_id': jQuery.data.user_id
		}
	})
	.done(function(data){
		console.log(data);

		if (data.error){
			jQuery('#login_status').text(data.msg).show();
			jQuery("input[name='twofapi-group']").prop('checked', false);
			jQuery('#twofapi-choose').show('fast');
		}
		else if (data.ip_address !== ajax_login_object.ip_address){
			alert("Security Error 7");
		}
		else {
			if (data.errors){
				var error_strgfy = JSON.stringify(data.errors);
				var error_str = error_strgfy.replace(/:/g," ");
				error_str = error_str.replace(/[^0-9a-zA-Z_ ]/g,"");
				jQuery('#login_status').text(error_str).show();
				jQuery("input[name='twofapi-group']").prop('checked', false);
				jQuery('#twofapi-choose').show('fast');
			}
			else if (data.result){
				jQuery('#login_status').text('Sending Device Authentication E-mail').show();
				jQuery("input[name='twofapi-group']").prop('checked', false);
				jQuery('#twofapi-choose').show('fast');
			}
			else{
				alert('Device Authentication Error');
			}
		}

	})
	.fail(function( jqXHR, textStatus ) {
		alert( "Request failed: " + textStatus );
	});
}


