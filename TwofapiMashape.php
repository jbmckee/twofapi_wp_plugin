<?php

/**
 * Class TwofapiMashape
 */
class TwofapiMashape extends TwoFAPI{

	// Interface with api
	/**
	 * @param $data
	 * @param $func
	 *
	 * @return array|mixed|object|WP_Error
	 */
	protected function mashape($data, $func){

		set_time_limit(350);

		$ip_address = $_SERVER['REMOTE_ADDR'];
		$mashape_token = get_option('mashape_token');
		$mashape_user = get_option('mashape_user');

		$sig = md5($data['user_token'].$mashape_user.$ip_address);

		$data["sig"] = $sig;
		$data["ip_address"] = $ip_address;

		$data["site_domain"] = get_home_url();

		$hdrs = [
			"x-mashape-key: {$mashape_token}",
			"content-type: application/x-www-form-urlencoded",
			"accept: application/json"
		];

		$url = 'https://2fapi.p.mashape.com/twofav1/twofa/'.$func;
		$urlencoded_str = http_build_query($data);

		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($curl, CURLOPT_HEADER, true);
		curl_setopt($curl, CURLOPT_HTTPHEADER, $hdrs);
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $urlencoded_str);
		curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curl, CURLOPT_TIMEOUT, 350);
		curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($curl, CURLOPT_ENCODING, '');

		$raw_response = trim(curl_exec($curl));

		if(curl_errno($curl)) {
			$error_message = curl_error($curl);
			return new WP_Error('api_failure', $error_message);

		}
		curl_close($curl);

		$json = preg_replace('/.+\n{/s','{', $raw_response);
		$response = json_decode($json);

		if (!empty($response->code)) return new WP_Error($response->code, $response->message);
		else return $response;
	}

	// Add new user through API
	/**
	 * @param $user_id
	 *
	 * @return array|mixed|object|WP_Error
	 */
	public function add_user($user_id){

		$cell_num = get_the_author_meta('cell_number', $user_id);
		$user = get_userdata($user_id);

		$mash_data = [
			"user_token" => $user->data->user_login,
			"email" => $user->data->user_email,
			"cell_num" => $cell_num,
		];

		return $this->mashape($mash_data, 'qrauth');
	}

	// Get QR code image in base64
	public function ajax_get_qr(){

		if (empty($_POST['token'])) {
			echo json_encode(['error' => true, 'msg' => 'Token missing']);
			wp_die();
		}

		$mash_data = [
			"user_token" => $_POST['token'],
		];

		$response = $this->mashape($mash_data, 'qrverify');

		if ($response->code == 400) {
			$msg = $response->get_error_message();
			echo json_encode(['error' => true, 'msg' => $msg]);
			wp_die();
		}

		$response->ip_address = $_SERVER['REMOTE_ADDR'];

		echo json_encode($response);

		wp_die();
	}

	// Check for QR code validation. Wait for response.
	public function ajax_wait_qr(){

		if (empty($_POST['token'])) {
			echo json_encode(['error' => true, 'msg' => 'Token missing']);
			wp_die();
		}

		if (empty($_POST['verify_key'])) {
			echo json_encode(['error' => true, 'msg' => 'Verify key missing']);
			wp_die();
		}

		$mash_data = [
			"user_token" => $_POST['token'],
			"verify_key" => $_POST['verify_key'],
			"wait" => $_POST['wait']
		];

		$response = $this->mashape($mash_data, 'qrcheck');

		if ($response->code == 400) {
			$msg = $response->get_error_message();
			echo json_encode(['error' => true, 'msg' => $msg]);
			wp_die();
		}

		if ($response->valid){
			// If QR code validates, handle cookie to remember device for 30 days
			$remember = get_option('2fapi_remember');
			if ($remember){
				$expires = time() + 30 * 60 * 60 * 24;
				setcookie('twofapi-'.$_POST['token'], $response->valid, $expires, COOKIEPATH, COOKIE_DOMAIN);
			}
		}

		$response->ip_address = $_SERVER['REMOTE_ADDR'];

		echo json_encode($response);

		wp_die();
	}

	// If no cell service, handle authentication by providing verification code key
	public function ajax_vcode(){

		if (empty($_POST['token'])) {
			echo json_encode(['error' => true, 'msg' => 'Token missing']);
			wp_die();
		}

		if (empty($_POST['verify_key'])) {
			echo json_encode(['error' => true, 'msg' => 'Verify key missing']);
			wp_die();
		}

		if (empty($_POST['vcode'])) {
			echo json_encode(['error' => true, 'msg' => '7 digit verification key missing']);
			wp_die();
		}

		$mash_data = [
			"user_token" => $_POST['token'],
			"verify_key" => $_POST['verify_key'],
			"validation_code" => $_POST['vcode']
		];

		$response = $this->mashape($mash_data, 'vcode');

		if ($response->code == 400) {
			$msg = $response->get_error_message();
			echo json_encode(['error' => true, 'msg' => $msg]);
			wp_die();
		}

		$response->ip_address = $_SERVER['REMOTE_ADDR'];

		echo json_encode($response);

		wp_die();
	}

	// Send SMS for 2 factor auth and wait for response
	public function ajax_do_sms(){

		if (empty($_POST['token'])) {
			echo json_encode(['error' => true, 'msg' => 'Token missing']);
			wp_die();
		}

		$mash_data = [
			"user_token" => $_POST['token']
		];

		if (!empty($_POST['verify_key'])) $mash_data['verify_key'] = $_POST['verify_key'];

		$response = $this->mashape($mash_data, 'smsverify');

		if ($response->code == 400) {
			$msg = $response->get_error_message();
			echo json_encode(['error' => true, 'msg' => $msg]);
			wp_die();
		}

		if ($response->valid){
			// If verified, handle setting cookie to remember device 30 days
			$remember = get_option('2fapi_remember');
			if ($remember){
				$expires = time() + 30 * 60 * 60 * 24;
				setcookie('twofapi-'.$_POST['token'], $response->valid, $expires, COOKIEPATH, COOKIE_DOMAIN);
			}
		}


		$response->ip_address = $_SERVER['REMOTE_ADDR'];

		echo json_encode($response);

		wp_die();
	}

	// Authorize mobile device app
	public function ajax_device_auth(){

		if (empty($_POST['user_id'])) {
			echo json_encode(['error' => true, 'msg' => 'User ID Missing']);
			wp_die();
		}

		$response = $this->add_user($_POST['user_id']);
		$response->ip_address = $_SERVER['REMOTE_ADDR'];

		echo json_encode($response);

		wp_die();

	}

	// Verify device has been authorized if remembering for 30 days
	/**
	 * @param $verify_key
	 * @param $user_token
	 *
	 * @return bool
	 */
	public function verify_remember($verify_key, $user_token){

		$mash_data = [
			"user_token" => $user_token,
			"verify_key" => $verify_key,
		];

		$response = $this->mashape($mash_data, 'remember');

		if ($response->code == 400) {
			$msg = $response->get_error_message();
			echo json_encode(['error' => true, 'msg' => $msg]);
			wp_die();
		}

		if (is_wp_error($response)) {
			$code = $response->get_error_code();
			$msg = $response->get_error_message($code);
			echo "Error: $code $msg";
			wp_die();
		}

		if (!empty($response->valid)) return true;
		else return false;

	}

}