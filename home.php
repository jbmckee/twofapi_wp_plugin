<?php

// Check if there are POST parameters and verify nonce
if ( ! empty( $_POST ) && check_admin_referer( 'manage-2fapi', '2fapi-nonce' ) ) {
    if (!empty($_POST['2fa-connect'])){
	    // Save Mashape token and username
	    if (strlen($_POST['mashape_token']) < 10)
            $msg = "<div style='font-size: 16px;' class='notice notice-error is-dismissible'><p>API Key Not Submitted</p></div>";
        elseif (strlen($_POST['mashape_user']) < 4){
	        $msg = "<div style='font-size: 16px;' class='notice notice-error is-dismissible'><p>Mashape Username Not Submitted</p></div>";
        }
        else {
	        update_option('mashape_token', $_POST['mashape_token']);
	        update_option('mashape_user', $_POST['mashape_user']);
	        $msg = "<div style='font-size: 16px;' class='notice notice-success is-dismissible'><p>API Key Updated</p></div>";
        }
    }
    else if (!empty($_POST['2fa-toggle'])){
        // Toggle 2 factor auth on or off
	    update_option('2fapi_on', $_POST['toggle-lockdown']);
	    if (empty($_POST['toggle-lockdown'])){
		    $msg = "<div style='font-size: 16px;' class='notice notice-success is-dismissible'><p>2FAPI Disabled</p></div>";
	    }
	    else{
		    $msg = "<div style='font-size: 16px;' class='notice notice-success is-dismissible'><p>2FAPI Enabled</p></div>";
	    }
    }
    else if (!empty($_POST['2fapi-authorize'])){
        // Authorize mobile device app for current user
	    $user = wp_get_current_user();
	    $return = $this->add_user($user->ID);
	    if (is_wp_error($return)){
	        $error = $return->get_error_message();
		    $msg = "<div style='font-size: 16px;' class='notice notice-error is-dismissible'><p>{$error}</p></div>";
	    }
	    else{
		    $msg = "<div style='font-size: 16px;' class='notice notice-success is-dismissible'><p>Authorization E-mail Sent</p></div>";
	    }

    }

    if (!empty($_POST['2fapi-check-remember'])){
        // Handle selection to remember device for 30 days
	    if (empty($_POST['remember'])){
		    update_option('2fapi_remember', 0);
		    $msg = "<div style='font-size: 16px;' class='notice notice-success is-dismissible'><p>Remember device Disabled</p></div>";
	    } else {
		    update_option('2fapi_remember', 1);
		    $msg = "<div style='font-size: 16px;' class='notice notice-success is-dismissible'><p>Remember Device Enabled</p></div>";
	    }
    }
}

$cur_state = get_option('2fapi_on');
$mashape_token = get_option('mashape_token');
$mashape_user = get_option('mashape_user');
$remember = get_option('2fapi_remember');

// Check if 2 factor auth is enabled
if (empty($cur_state)){
    if (empty($msg)) $msg = '<div style="font-size: 16px;" class="notice notice-error is-dismissible"><p>Two Factor Authentication Not Active</p></div>';
    $toggle_lockdown = 1;
    $form_submit = 'Enable';
}
else {
    // Check if deactived in config
	if (defined('TWOFA_ACTIVE') && TWOFA_ACTIVE){
		if (empty($msg)) $msg = '<div style="font-size: 16px;" class="notice notice-warning is-dismissible"><p>Two Factor Authentication Active</p></div>';
		$toggle_lockdown = 0;
		$form_submit = 'Disable';
	}
	else {
		if (empty($msg)){
			$msg = '<div style="font-size: 16px;" class="notice notice-error is-dismissible"><p>Two Factor Authentication Disabled in Config</p></div>';
		}
		$form_submit = 'Invalid';
	}
}

?>

<div class="wrap" id="2fapi" style="max-width:950px !important;">
    <h2>2FAPI Two Factor Authentication</h2>

	<div id="mainblock" style="width:710px">
		<?php echo $msg ?>

        <p>
            <a href="https://2fapi.app" target="_blank"><h3>Click Here for 2FAPI Info</h3></a>
        </p>
        <p>
        <form method="post" id="2fa-connect">

            <div class="form-group 2fa-key has-success">
                <label class="control-label" for="2fa-key">Mashape/RapidAPI Key</label>
                <input type="text" id="mashape_token" class="form-control" name="mashape_token" maxlength="50" aria-invalid="false" value="<?=$mashape_token?>">
            </div>

            <div class="form-group 2fa-key has-success">
                <label class="control-label" for="2fa-key">Mashape Username</label>
                <input type="text" id="mashape_user" class="form-control" name="mashape_user" maxlength="50" aria-invalid="false" value="<?=$mashape_user?>">
            </div>

            <input type="hidden" name="2fa-connect" id="" value="1">

            <?php submit_button( "Submit API Credentials"); ?>
            <?php wp_nonce_field( 'manage-2fapi', '2fapi-nonce' ); ?>
        </form>
       </p>

        <?php if (!empty($mashape_token) && !empty($mashape_user) && ($form_submit !== 'Invalid')): ?>
        <p>
        <form method="post">
            <input type="hidden" name="toggle-lockdown" id="toggle-lockdown" value="<?=$toggle_lockdown?>">
            <input type="hidden" name="2fa-toggle" id="" value="1">
            <?php submit_button( "$form_submit Two Factor Authentication"); ?>
            <?php wp_nonce_field( 'manage-2fapi', '2fapi-nonce' ); ?>
        </form>
        </p>

        <p>
        <form method="post">
            <input type="hidden" name="2fapi-authorize" id="" value="1">
            <?php submit_button( "Authorize Device"); ?>
            <?php wp_nonce_field( 'manage-2fapi', '2fapi-nonce' ); ?>
        </form>
        </p>
        <?php endif; ?>

         <p>If you end up locked out of your account due to two factor authentication failure, the 2FAPI plugin can be disabled in the wp-config file. You will need FTP access to modify this file. Find where the PHP constant TWOFA_ACTIVE is defined and set the value to zero. This will disable two factor authentication. Remember to set it back to value of 1 when the issue is resolved.</p>

        <form method="post">
            <input type="hidden" name="2fapi-check-remember" id="" value="1">

    <table>
        <tr>
            <th scope="row">Enable Remember Device</th>
            <td id="remember_text">
                <input name="remember" type="checkbox" id="remember" value="1" <?=(!empty($remember)?'checked':'')?>>
                    Check this box to allow 2AFPI to remember the users device and not re-authorize logins on that device for 30 days.
            </td>
        </tr>
    </table>
	        <?php submit_button( "Save Remember Device"); ?>
	        <?php wp_nonce_field( 'manage-2fapi', '2fapi-nonce' ); ?>

        </form>
    </div>
</div>
