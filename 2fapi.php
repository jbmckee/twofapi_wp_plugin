<?php
/*
Plugin Name: 2FAPI
Version: 1.0
Description: Robust Two Factor Authentication for protecting login.
Author: Egg Sandwich Software
Author URI: http://eggsoft.tech
*/

/* Prevent direct access to the plugin */
if ( ! defined( 'ABSPATH' ) ) exit;

if (!class_exists('TwoFAPI')):

	/**
	 * Class TwoFAPI
	 */
	class TwoFAPI {

	    private $mashape_token;
		private $update_2fapi_error;


		/**
		 * TwoFAPI constructor.
		 */
		function __construct() {
			if ( is_admin() ) {
				add_action( 'admin_menu', array( &$this, 'links_menu' ) );
			}

			register_activation_hook(__FILE__,array(&$this,'on_activate'));
			register_deactivation_hook(__FILE__,array(&$this,'on_deactivate'));

			if (defined('TWOFA_ACTIVE') && TWOFA_ACTIVE){
				$cur_state = get_option('2fapi_on');
				$this->mashape_token = get_option('mashape_token');
				if (!empty($cur_state) && !empty($this->mashape_token)){
					add_filter('login_form', array(&$this, 'login_2fa_field'), 1);
					add_action('login_enqueue_scripts', array(&$this, 'login_page'));
				}
			}

			add_action( 'personal_options_update', array(&$this,'on_profile_update'));
			add_action( 'edit_user_profile_update', array(&$this,'on_profile_update'));

			add_action('user_profile_update_errors', array(&$this,'save_cell_number_fields'), 10, 3);

			add_action( 'show_user_profile', array(&$this,'cell_user_field'));
			add_action( 'edit_user_profile', array(&$this,'cell_user_field'));
			add_action( 'user_new_form', array(&$this,'cell_user_field'));

			add_action( 'wp_ajax_nopriv_ajaxlogin', array(&$this,'ajax_login') );
			add_action( 'wp_ajax_ajaxlogin', array(&$this,'ajax_already_login') );
			add_action( 'wp_ajax_nopriv_ajax_get_qr', array(&$this,'ajax_get_qr') );
			add_action( 'wp_ajax_nopriv_ajax_wait_qr', array(&$this,'ajax_wait_qr') );
			add_action( 'wp_ajax_nopriv_ajax_real_login', array(&$this,'ajax_real_login') );
			add_action( 'wp_ajax_nopriv_ajax_vcode', array(&$this,'ajax_vcode') );
			add_action( 'wp_ajax_nopriv_ajax_do_sms', array(&$this,'ajax_do_sms') );
			add_action( 'wp_ajax_nopriv_ajax_device_auth', array(&$this,'ajax_device_auth') );

		}

		// Add 2FAPI to tools menu
		function links_menu() {
			$main_menu = add_management_page( '2FAPI Two Factor Authentication', '2FAPI', 'administrator', '2FAPI', array(
					&$this,
					'home'
				)
			);

			add_action("admin_print_scripts-$main_menu", array(&$this, 'admin_head'));
		}

		// If user is already logged in, forward to dashboard
		function ajax_already_login(){
			$data = [
			        'already'=>true,
			        'ip_address' => $_SERVER['REMOTE_ADDR']
			];
			echo json_encode($data);
			wp_die();
        }

        // Do first login check of user credentials
		function ajax_login(){

			check_ajax_referer( 'ajax-login-nonce', 'security' );

			$info = array();
			$info['user_login'] = $_POST['username'];
			$info['user_password'] = $_POST['password'];
			$info['remember'] = $_POST['rememberme'];

			$user_signon = wp_signon( $info, false );

			if ( is_wp_error($user_signon) ){
			    // Generally invalid credentials
			    $msg = $user_signon->get_error_message();
				echo json_encode([
				        'loggedin'=>false,
                        'message'=> $msg,
				        'ip_address' => $_SERVER['REMOTE_ADDR']
				]);
			} else {

				$user_data = [
				    'user_id' => $user_signon->ID,
                    'token' => $user_signon->user_login,
                    'loggedin' => true,
				    'ip_address' => $_SERVER['REMOTE_ADDR']
                ];

				// Handle rememering device for 30 days
				$remember = get_option('2fapi_remember');
				if ($remember){
  $cookie_name = 'twofapi-'.$user_signon->user_login;
				    $verify_key = empty($_COOKIE[$cookie_name])?null:$_COOKIE[$cookie_name];

				    if ($verify_key){

				        $active = $this->verify_remember($verify_key, $user_signon->user_login);

					    if ($active){
					        $user_data['already'] = true;
						    echo json_encode($user_data);
						    wp_die();
					    }
				    }
                }

                // Log out to perform 2 factor auth
				wp_logout();

				echo json_encode($user_data);
			}

			wp_die();
		}

		// If 2 factor auth valid, do real login and forward to dashboard
		function ajax_real_login(){

		    if ($_POST['security'] !== $_SERVER['REMOTE_ADDR']){
			    $msg = 'Security Error';
			    echo json_encode(array('loggedin' => false, 'message' => $msg));
		    }

			$info = array();
			$info['user_login'] = $_POST['username'];
			$info['user_password'] = $_POST['password'];
			$info['remember'] = $_POST['rememberme'];

			$user_signon = wp_signon( $info, false );

			if ( is_wp_error($user_signon) ){
				$msg = $user_signon->get_error_message();
				echo json_encode([
				        'loggedin' => false,
                        'message' => $msg,
                        'ip_address' => $_SERVER['REMOTE_ADDR']
                ]);
			} else {
				echo json_encode([
				        'loggedin' => true,
                        'ip_address' => $_SERVER['REMOTE_ADDR']
                    ]);
			}

			wp_die();

		}

		// On activating plugin define constant in config
		function on_activate(){
			$fs_method = get_filesystem_method();
			if ($fs_method != 'direct'){
				die('Plugin cannot access filesystem directly.');
			}
			if (!WP_Filesystem()){
				die('Plugin error connecting to filesystem.');
			}

			global $wp_filesystem;

			$path = get_home_path();
			$config_file_path = "$path/wp-config.php";

			$config_file = $wp_filesystem->get_contents($config_file_path);
			$clear_config = $this->remove_config($config_file);

			$new_config = preg_replace("/(define\(.DB_NAME.,.+\);)/Ui","$1\n\ndefine('TWOFA_ACTIVE', 1);\n",$clear_config);

			if ( ! $wp_filesystem->put_contents( $config_file_path, $new_config) ) {
				die('error updating config file');
			}

		}

		// On plugin deactive remove constant from config
		function on_deactivate(){
			$fs_method = get_filesystem_method();
			if ($fs_method != 'direct'){
				return;
			}
			if (!WP_Filesystem()){
				return;
			}

			global $wp_filesystem;
			$path = get_home_path();
			$config_file_path = "$path/wp-config.php";

			$config_file = $wp_filesystem->get_contents($config_file_path);
			$clear_config = $this->remove_config($config_file);

			$wp_filesystem->put_contents( $config_file_path, $clear_config);
		}

		/**
		 * @param $config_file
		 *
		 * @return null|string|string[]
		 */
		function remove_config($config_file){
			$return_file = preg_replace("/define\(.TWOFA_ACTIVE.,.+\);/Ui",'',$config_file);
			return $return_file;
		}

		// Enqueue scripts and styles for admin area
		function admin_head(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', array('jquery'));
			wp_enqueue_script('2fa_loading_modal_js', plugins_url('/2fapi/js/jquery.loadingModal.min.js'), array('jquery'));
			wp_enqueue_script('scripts', plugins_url('/2fapi/js/scripts.js'), array('jquery','jquery-ui'));

			wp_enqueue_style('admin_css_jquery-ui', '//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css', false, null, 'all');
			wp_enqueue_style('admin_css_2fapi', plugins_url('/2fapi/css/style.css'), false, false, 'all');
			wp_enqueue_style('2fa_loading_modal_css', plugins_url('/2fapi/css/jquery.loadingModal.min.css'), false, false, 'all');

		}

		// Display management page
		function home(){
			include 'home.php';
		}

		// Add the 2 factor auth fields to login form
		function login_2fa_field() {
            ?>
<h3 id="login_status" style="margin-bottom: 10px;"></h3>
<div id="twofapi-choose">

    <p><b>Select method of second factor authentication.</b></p>
    <p><input type="radio" name="twofapi-group" value="sms"> Send SMS</p>
    <p><input type="radio" name="twofapi-group" value="qr"> QR Code</p>
    <p><input type="radio" name="twofapi-group" value="auth"> Re-Authorize Device</p>

</div>

<div id="twofapi-qr-show">
    <div>
        <img src="" id="twofapi-qr-img">
    </div>
    <div class="button-div">
        <a href="javascript:void(0)" class="button button-primary button-large" id="twofapi_direct">Tap here if this device authorized.</a>
    </div>
    <div class="button-div" id="twofapi-show-check-link">
        <div>Auto auth timed out.</div>
        <a href="javascript:void(0)" class="button button-primary button-large" class="twofapi_check">Click here to verify.</a>
    </div>
    <div id="twofapi-show-input-link" class="clear-row">
        <a href="javascript:void(0)" id="twofapi-show-input">Click here if no cell service</a>
    </div>
    <div id="twofapi-show-codebox">
        <input type='text' name='twofapi-vcode' id='twofapi-vcode' size='7' maxlength='7' value='' placeholder="Enter 7 digit code"><br>
        <a href="javascript:void(0)" class="button button-primary button-large" id="twofapi_verify_vcode">Click here to verify.</a>

    </div>
</div>

<div class="button-div" id="twofapi-show-sms-check">
    <div class="sms-msg">You can still click the link in the sms message and then click this button.</div>
    <a href="javascript:void(0)" class="button button-primary button-large twofapi_check">Click here to verify</a>
</div>

			<?php
		}

		// Enqueue scripts and styles for login page
		function login_page(){
			wp_enqueue_script('2fa_loading_modal_js', plugins_url('/2fapi/js/jquery.loadingModal.min.js'), array('jquery'));
			wp_enqueue_script('2fa_login_js', plugins_url('/2fapi/js/scripts.js'), array('jquery'));
			wp_enqueue_style('2fa_login_css', plugins_url('/2fapi/css/login_style.css'), false, false, 'all');

			wp_localize_script( '2fa_login_js', 'ajax_login_object', array(
				'ajaxurl' => admin_url( 'admin-ajax.php' ),
				'redirecturl' => admin_url(),
				'ip_address' => $_SERVER['REMOTE_ADDR'],
                'security' => wp_create_nonce('ajax-login-nonce')
			));
		}

		// Add cell phone field to user profile

		/**
		 * @param string $user
		 */
		function cell_user_field($user = ''){
			$cur_val = '';
			if (!empty($user->ID)) $cur_val = get_the_author_meta( 'cell_number', $user->ID );
			?>
            <h3>Two Factor Authentication By Text Message</h3>

            <table class="form-table">
                <tr>
                    <th><label for="cell_number">Cell Phone Number</label></th>
                    <td>
                        <input type="text" name="cell_num" id="cell_num" value="<?=$cur_val?>" class="regular-text"  /><br />
                        <span class="description"><?php _e("Please enter cell number including country code to enable two factor authentication by text message."); ?></span>
                    </td>
                </tr>
                <tr>
                    <th scope="row">Invalidate Device</th>
                    <td><fieldset><legend class="screen-reader-text"><span>Invalidate Device</span></legend>
                            <label for="invalidate_device">
                                <input name="invalidate_device" type="checkbox" id="invalidate_device" value="1">
                                Check this box to invalidate device for this user.</label><br>
                        </fieldset>
                    </td>
                </tr>
            </table>

			<?php
		}

		// Save the cell number of user

		/**
		 * @param $errors
		 * @param $update
		 * @param $user
		 */
		function save_cell_number_fields( $errors, $update, $user ) {

			if (empty($_POST['cell_num'])){
				$errors->add('cell_missing',__('Cell number is required.'));
                return;
			}

			if (!empty($this->update_2fapi_error)){
				$errors->add('email_api_error',$this->update_email_error);
				return;
            }

			$cur_val = get_the_author_meta( 'cell_number', $user->ID );
			$cell_num = preg_replace('/\D/','',$_POST['cell_num']);
			if (!empty($cur_val) && ($cur_val != $cell_num)){
			    update_user_meta( $user->ID, 'cell_number', $cell_num );

				$mash_data = [
					"user_token" => $user->user_login,
					"cell_num" => $cell_num,
				];

				$response = $this->mashape($mash_data, 'updatecell');

				if (is_wp_error($response)) {
				    $code = $response->get_error_code();
				    $msg = $response->get_error_message($code);
					$errors->add($code,$msg);
				}

			}
		}

		// Fire when updating profile
		/**
		 * @param $user_id
		 */
		function on_profile_update($user_id){
		    $this->invalidate_device($user_id);
		    $this->save_email_field($user_id);
        }

        // Handle request to invalidate device
		/**
		 * @param $user_id
		 */
		function invalidate_device($user_id){
			$user = get_userdata($user_id);

			if (!empty($_POST['invalidate_device'])){

				$mash_data = [
					"user_token" => $user->user_login,
				];

				$response = $this->mashape($mash_data, 'invalidate');

				if (is_wp_error($response)) {
					$code = $response->get_error_code();
					$msg = $response->get_error_message($code);
					$this->update_2fapi_error = $msg;
				}

			}
        }

        // Handle changing email address
		/**
		 * @param $user_id
		 */
		function save_email_field($user_id) {

			$user = get_userdata($user_id);

			if ($user->data->user_email != $_POST['email']){

				$mash_data = [
					"user_token" => $user->user_login,
					"email" => $_POST['email'],
				];

				$response = $this->mashape($mash_data, 'updateemail');

				if (is_wp_error($response)) {
					$code = $response->get_error_code();
					$msg = $response->get_error_message($code);
					$this->update_2fapi_error = $msg;
				}

			}
		}


	}

	include 'TwofapiMashape.php';

endif;

$TwoFAPI = new TwofapiMashape();
